import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Observable, of } from 'rxjs';
import { catchError, filter, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BERdataService {
  public members: any = [];

  constructor(private http: HttpClient) { 
    this.getMembers().subscribe(res =>{
      console.log(res)
      this.members = res;
    });

   }

   getMembers(): Observable<BERdataService[]>{
    return this.http.get<BERdataService[]>("http://202.179.135.250/api/member_directory/5ec4c0c06d937d001131296b")
    .pipe(
      tap(Member => console.log('Users List Received')),
      catchError(this.handleError<BERdataService[]>('Get Users', []))

   );

   }
   private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  
  }
  filterItems(searchTerm) {
    return this.members.filter(member => {
      return member.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    }

    

    
    )
  }
  
}
