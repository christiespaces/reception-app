import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {  path: 'check-in',
        loadChildren: () => import( '../check-in/check-in.module').then(m => m.CheckInPageModule)
    },
    {  path: 'contractor-in',
    loadChildren: () => import( '../contractor-in/contractor-in.module').then(m => m.ContractorInPageModule)
    },
    {  path: 'contractor-out',
    loadChildren: () => import( '../contractor-out/contractor-out.module').then(m => m.ContractorOutPageModule)
    },
    {  path: 'mail',
    loadChildren: () => import( '../mail/mail.module').then(m => m.MailPageModule)
    },
    {
      path: 'qr',
      loadChildren: () => import('../qr/qr.module').then( m => m.QrPageModule)
    },
    {
      path: '',
      redirectTo: '/locations/berry-st/check-in',
      pathMatch: 'full',
    }

    ]
  },
  {
    path: '',
    redirectTo: '/locations/berry-st/check-in',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
