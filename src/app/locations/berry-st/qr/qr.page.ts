import { Component, OnInit } from '@angular/core';

import {HttpClient} from "@angular/common/http";
import { Router } from '@angular/router';

@Component({
  selector: 'app-qr',
  templateUrl: './qr.page.html',
  styleUrls: ['./qr.page.scss'],
})
export class QrPage implements OnInit {

  constructor(http: HttpClient, private router: Router) {


   }

  ngOnInit() {
  }
  inducted(){
    this.router.navigate(['/locations/berry-st/contractor-in']);
    }
}

