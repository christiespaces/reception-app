import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Router } from '@angular/router';

@Component({
  selector: 'app-contractor-out',
  templateUrl: './contractor-out.page.html',
  styleUrls: ['./contractor-out.page.scss'],
})
export class ContractorOutPage implements OnInit {

  contractor = {
    firstName: '',
    lastName: ''
  };
  http : HttpClient;

  constructor(http: HttpClient, private router: Router) {
    this.http = http;
   }

  ngOnInit() {
 
  }
  signOut(form){
  
    fetch('https://api.smtp2go.com/v3/email/send', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "api_key": "api-38B578D6B91C11EBA64CF23C91C88F4E",
        "to": ["steve@christiespaces.com.au", "sandra@christiespaces.com.au"],
        "sender": "Christie Spaces Reception <no-reply@christiespaces.com.au>",
        "subject": "Contractor" + " " + this.contractor.firstName + " " + this.contractor.lastName + " " + "has signed out. BERRY STREET",
        "html_body": "Contractor" + " " + this.contractor.firstName + " " + this.contractor.lastName + " " + "has signed out.",
      })
    }).then(function (response) {
      return response.json();
    });
    console.log(this.contractor.firstName, this.contractor.lastName, ),
      this.router.navigate(['/locations/contractor-out']);
  }

}
