import { Component, Input, OnInit } from '@angular/core';
import { BERdataService } from '../../../services/berdata.service'
import { FormControl, } from "@angular/forms";
import { debounceTime } from "rxjs/operators";
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';


export interface selectedMembersData {
  name: string;
  company_name: string;
  phone: string;
  email: string;

}

var selectedMembersData: any;

@Component({
  selector: 'app-check-in',
  templateUrl: './check-in.page.html',
  styleUrls: ['./check-in.page.scss'],
})
export class CheckInPage implements OnInit {
  searchTerm: string = '';
  searchControl: FormControl;
  BERmembers: any;
  searching: any = false;
  searchResults: any = false;

  selectedMembersData: any;

  // Collects Guest Info to be emailed to Memeber
  visitor = {
    firstName: '',
    lastName: '',
    company: '',
    phoneNumber: '',
  };



  constructor(private dataService: BERdataService, private router: Router, private _activatedRoute: ActivatedRoute) {
    this.searchControl = new FormControl();
    this._activatedRoute.paramMap.subscribe(params => {
      this.ngOnInit();
    });

  }

  ngOnInit() {
    this.setFilteredItems("");

    this.searchControl.valueChanges.pipe(debounceTime(700))
      .subscribe(search => {
        this.searching = false;
        this.searchResults = true;
        this.setFilteredItems(search);
      });
  }

  onSearchInput() {
    this.searching = true;
    this.searchResults = false;
  }
  setFilteredItems(searchTerm) {
    this.BERmembers = this.dataService.filterItems(searchTerm);
  }

  //Selecting the Member
  clickMember(member) {
    this.selectedMembersData = member;
    console.log(this.selectedMembersData)
  }

  onClear() {
    setTimeout(() => {
      this.searchResults = false;
    },
      1000);
  }

  onCancel() {
    setTimeout(() => {
      this.searchResults = false;
    },
      1000);
  }



  visitorIn(form) {
    if (this.selectedMembersData.phone_geocode == "AU Mobile") {
      console.log("Mobile is Valid")

      // fetch('127.0.0.1',{
      // Alert via SMS
      fetch('http://202.179.135.250/api/sms/send', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',

        },
        body: JSON.stringify({
          "token": "38d997ef-bfef-447e-9e04-9d019bdabd56",
          "to": this.selectedMembersData.phone,
          "from": "Reception",
          "body": this.visitor.firstName + " " + this.visitor.lastName + " " + "from" + " " + this.visitor.company + " " + "is waiting for you in reception. They can be contacted on" + " " + "0" + this.visitor.phoneNumber,
        })
      });
      
      //Alert Via Email
      fetch('https://api.smtp2go.com/v3/email/send', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "api_key": "api-38B578D6B91C11EBA64CF23C91C88F4E",
          "to": [this.selectedMembersData.email_address],
          "sender": "Christie Spaces Reception <no-reply@christiespaces.com.au>",
          "subject": "New Visitor Sign In",
          "html_body": this.visitor.firstName + " " + this.visitor.lastName + " " + "from" + " " + this.visitor.company + " " + "is waiting for you in reception. They can be contacted on" + " " + "0" + this.visitor.phoneNumber,

        })

      }).then(function (response) {
        return response.json();

      });

    this.router.navigate(["/locations/visitor"]);
    } else
      //Alert Via Email
      fetch('https://api.smtp2go.com/v3/email/send', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "api_key": "api-38B578D6B91C11EBA64CF23C91C88F4E",
          "to": [this.selectedMembersData.email_address],
          "sender": "Christie Spaces Reception <no-reply@christiespaces.com.au>",
          "subject": "New Visitor Sign In",
          "html_body": this.visitor.firstName + " " + this.visitor.lastName + " " + "from" + " " + this.visitor.company + " " + "is waiting for you in reception. They can be contacted on" + " " + "0" + this.visitor.phoneNumber,

        })

      }).then(function (response) {
        return response.json();

      });

    this.router.navigate(["/locations/visitor"]);
  }
}
