import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LeasingPage } from './leasing.page';

const routes: Routes = [
  {
    path: '',
    component: LeasingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeasingPageRoutingModule {}
