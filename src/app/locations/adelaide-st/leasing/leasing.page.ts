import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { UserIdleService } from 'angular-user-idle';

@Component({
  selector: 'app-leasing',
  templateUrl: './leasing.page.html',
  styleUrls: ['./leasing.page.scss'],
})
export class LeasingPage implements OnInit {

 
  // Collects Guest Info to be emailed to Memeber
  visitor = {
    firstName: '',
    lastName: '',
    company: '',
    phoneNumber: '',
  }



  constructor(private router: Router, private _activatedRoute: ActivatedRoute, private userIdle: UserIdleService,  ) { }
  ngOnInit() {
    //Start watching for user inactivity.
    this.userIdle.startWatching();
    
    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe( );
    
    // Start watch when time is up Redirect to Root and Reset Time.
    this.userIdle.onTimeout().subscribe(() => {
    this.userIdle.stopWatching();
    this.userIdle.resetTimer();
    this.router.navigate(["/"])
   }); 
  
  }

  leasing(leasingform) {
   
      console.log("Mobile is Valid")

      // fetch('127.0.0.1',{
      // Alert via SMS
      fetch('http://202.179.135.250/api/sms/send', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',

        },
        body: JSON.stringify({
          "token": "38d997ef-bfef-447e-9e04-9d019bdabd56",
          "to": "+61411879333",
          "from": "Reception",
          "body": this.visitor.firstName + " " + this.visitor.lastName + " " + "is in reception and has assistance with a leasing enquiry. They can be contacted on" + " " + "0" + this.visitor.phoneNumber,
        })
      });
      


    this.router.navigate(["/locations/leasing"]);
  }


}




