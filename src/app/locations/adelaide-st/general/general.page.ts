import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-general',
  templateUrl: './general.page.html',
  styleUrls: ['./general.page.scss'],
})
export class GeneralPage  {


  // Collects Guest Info to be emailed to Memeber
  visitor = {
    firstName: '',
    lastName: '',
    company: '',
    phoneNumber: '',
  };



  constructor(private router: Router, private _activatedRoute: ActivatedRoute) { }

  general(generalform) {
   
    fetch('https://api.smtp2go.com/v3/email/send', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "api_key": "api-38B578D6B91C11EBA64CF23C91C88F4E",
        "to": ["adelaidestreet@christiespaces.com.au"],
        "sender": "Christie Spaces Reception <no-reply@christiespaces.com.au>",
        "subject": "Assistance Requested",
        "html_body": this.visitor.firstName + " " + this.visitor.lastName + " " + "from" + " " + this.visitor.company + " " + "is in reception and has requested assistance. They can be contacted on" + " " + "0" + this.visitor.phoneNumber,

      })

    }).then(function (response) {
      return response.json();

    });


  this.router.navigate(["/locations/help"]);
}
}
