import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'check-in',
        loadChildren: () => import('../check-in/check-in.module').then(m => m.CheckInPageModule)
      },
      {
        path: 'general',
        loadChildren: () => import('../general/general.module').then(m => m.GeneralPageModule)
      },
      {
        path: '',
        redirectTo: '/locations/queen-st/check-in',
        pathMatch: 'full',
      }

    ]
  },
  {
    path: '',
    redirectTo: '/locations/queen-st/check-in',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule { }
