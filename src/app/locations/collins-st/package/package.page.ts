import { Component, Input, OnInit } from '@angular/core';
import { MELdataService } from '../../../services/meldata.service'
import { FormControl, } from "@angular/forms";
import { debounceTime } from "rxjs/operators";
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';


export interface selectedMembersData {
  name: string;
  company_name: string;
  phone: string;
  email_address: string;

}

var selectedMembersData: any;

@Component({
  selector: 'app-package',
  templateUrl: './package.page.html',
  styleUrls: ['./package.page.scss'],
})
export class PackagePage implements OnInit {
  searchTerm: string = '';
  searchControl: FormControl;
  MELmembers: any;
  searching: any = false;
  searchResults: any = false;

  selectedMembersData: any;

  constructor(private dataService: MELdataService, private router: Router, private _activatedRoute: ActivatedRoute) {
    this.searchControl = new FormControl();
    this._activatedRoute.paramMap.subscribe(params => {
      this.ngOnInit();
    });

  }

  ngOnInit() {
    this.setFilteredItems("");

    this.searchControl.valueChanges.pipe(debounceTime(700))
      .subscribe(search => {
        this.searching = false;
        this.searchResults = true;
        this.setFilteredItems(search);
      });
  }

  onSearchInput() {
    this.searching = true;
    this.searchResults = false;
  }
  setFilteredItems(searchTerm) {
    this.MELmembers = this.dataService.filterItems(searchTerm);
  }

  //Selecting the Member
  clickMember(member) {
    this.selectedMembersData = member;
    console.log(this.selectedMembersData)
  }

  onClear() {
    setTimeout(() => {
      this.searchResults = false;
    },
      1000);
  }

  onCancel() {
    setTimeout(() => {
      this.searchResults = false;
    },
      1000);
  }



  packageIn(packageForm) {
       //Alert Via Email
      fetch('https://api.smtp2go.com/v3/email/send', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "api_key": "api-38B578D6B91C11EBA64CF23C91C88F4E",
          "to": [ this.selectedMembersData.email_address ],
          "sender": "Christie Spaces Reception <no-reply@christiespaces.com.au>",
          "subject": "Package waiting in Reception",
          "html_body": "Hi " + this.selectedMembersData.name + "," + "<br>" + "<br>" + "There is a Package Delivery for you in Reception" + "<br>" + "<br>" + "Christie Spaces Reception" ,

        })

      }).then(function (response) {
        return response.json();

      });

    this.router.navigate(["/locations/mail"]);
  }
}
