import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-general',
  templateUrl: './general.page.html',
  styleUrls: ['./general.page.scss'],
})
export class GeneralPage {


  // Collects Guest Info to be emailed to Memeber
  visitor = {
    firstName: '',
    lastName: '',
    company: '',
    phoneNumber: '',
  };



  constructor(private router: Router, private _activatedRoute: ActivatedRoute) {

 

  }

  general(generalform) {
   
      console.log("Mobile is Valid")

      // fetch('127.0.0.1',{
      // Alert via SMS
      fetch('http://202.179.135.250/api/sms/send', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',

        },
        body: JSON.stringify({
          "token": "38d997ef-bfef-447e-9e04-9d019bdabd56",
          "to": "+61425301073",
          "from": "Reception",
          "body": this.visitor.firstName + " " + this.visitor.lastName + " " + "is in reception and has requested assistance. They can be contacted on" + " " + "0" + this.visitor.phoneNumber,
        })
      });
      


    this.router.navigate(["/locations/help"]);
  }
}
