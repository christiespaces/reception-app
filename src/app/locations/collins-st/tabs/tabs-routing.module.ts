import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'check-in',
        loadChildren: () => import('../check-in/check-in.module').then(m => m.CheckInPageModule)
      },
      {
        path: 'general',
        loadChildren: () => import('../general/general.module').then(m => m.GeneralPageModule)
      },
      {
        path: 'package',
        loadChildren: () => import('../package/package.module').then(m => m.PackagePageModule)
      },
      {
        path: 'mail',
        loadChildren: () => import('../mail/mail.module').then(m => m.MailPageModule)
      },
      {
        path: '',
        redirectTo: '/locations/collins-st/check-in',
        pathMatch: 'full',
      }

    ]
  },
  {
    path: '',
    redirectTo: '/locations/collins-st/check-in',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule { }
