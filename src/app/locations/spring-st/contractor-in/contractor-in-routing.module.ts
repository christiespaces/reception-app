import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContractorInPage } from './contractor-in.page';

const routes: Routes = [
  {
    path: '',
    component: ContractorInPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContractorInPageRoutingModule {}
