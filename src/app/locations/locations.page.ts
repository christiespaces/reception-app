import { Component, OnInit } from '@angular/core';
import { UserIdleService } from 'angular-user-idle';
import { Router } from '@angular/router';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.page.html',
  styleUrls: ['./locations.page.scss'],
})
export class LocationsPage implements OnInit {

  constructor(private userIdle: UserIdleService, private router: Router) { }

  ngOnInit() {

    //Start watching for user inactivity.
   this.userIdle.startWatching();
      
   // Start watching when user idle is starting.
   this.userIdle.onTimerStart().subscribe( );
   
   // Start watch when time is up Redirect to Root and Reset Time.
   this.userIdle.onTimeout().subscribe(() => {
   this.userIdle.stopWatching();
   this.userIdle.resetTimer();
   this.router.navigate(["/"])
  }); 
  }
  
  }