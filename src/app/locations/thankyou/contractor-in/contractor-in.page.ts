import { Component, OnInit } from '@angular/core';
import { UserIdleService } from 'angular-user-idle';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contractor-in',
  templateUrl: './contractor-in.page.html',
  styleUrls: ['./contractor-in.page.scss'],
})
export class ContractorInPage implements OnInit {
  constructor(private userIdle: UserIdleService, private router: Router) { }

  ngOnInit() {

   //Start watching for user inactivity.
  this.userIdle.startWatching();
     
  // Start watching when user idle is starting.
  this.userIdle.onTimerStart().subscribe( );
  
  // Start watch when time is up Redirect to Root and Reset Time.
  this.userIdle.onTimeout().subscribe(() => {
  this.userIdle.stopWatching();
  this.userIdle.resetTimer();
  this.router.navigate(["/"])
 }); 
 }
 
 }
 