import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LeasingPageRoutingModule } from './leasing-routing.module';

import { LeasingPage } from './leasing.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LeasingPageRoutingModule
  ],
  declarations: [LeasingPage]
})
export class LeasingPageModule {}
