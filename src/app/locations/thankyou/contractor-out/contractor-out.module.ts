import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContractorOutPageRoutingModule } from './contractor-out-routing.module';

import { ContractorOutPage } from './contractor-out.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContractorOutPageRoutingModule
  ],
  declarations: [ContractorOutPage]
})
export class ContractorOutPageModule {}
