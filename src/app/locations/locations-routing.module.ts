import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LocationsPage } from './locations.page';

const routes: Routes = [
  {
    path: '',
    component: LocationsPage
  },
  {
    path: 'adelaide-st',
    loadChildren: () => import('./adelaide-st/tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'queen-st',
    loadChildren: () => import('./queen-st/tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'berry-st',
    loadChildren: () => import('./berry-st/tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'walker-st',
    loadChildren: () => import('./walker-st/tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'spring-st',
    loadChildren: () => import('./spring-st/tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'collins-st',
    loadChildren: () => import('./collins-st/tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'visitor',
    loadChildren: () => import('./thankyou/visitor/visitor.module').then( m => m.VisitorPageModule)
  },
  {
    path: 'help',
    loadChildren: () => import('./thankyou/help/help.module').then( m => m.HelpPageModule)
  },
  {
    path: 'mail',
    loadChildren: () => import('./thankyou/mail/mail.module').then( m => m.MailPageModule)
  },
  {
    path: 'leasing',
    loadChildren: () => import('./adelaide-st/leasing/leasing.module').then( m => m.LeasingPageModule)
  },
  {
    path: 'leasing',
    loadChildren: () => import('./thankyou/leasing/leasing.module').then( m => m.LeasingPageModule)
  },
  {
    path: 'contractor-out',
    loadChildren: () => import('./thankyou/contractor-out/contractor-out.module').then( m => m.ContractorOutPageModule)
  },
  {
    path: 'contractor-in',
    loadChildren: () => import('./thankyou/contractor-in/contractor-in.module').then( m => m.ContractorInPageModule)
  },
 





];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LocationsPageRoutingModule {}
