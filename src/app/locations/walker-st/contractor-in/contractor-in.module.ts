import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContractorInPageRoutingModule } from './contractor-in-routing.module';

import { ContractorInPage } from './contractor-in.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContractorInPageRoutingModule
  ],
  declarations: [ContractorInPage]
})
export class ContractorInPageModule {}
