import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Router } from '@angular/router';

@Component({
  selector: 'app-contractor-in',
  templateUrl: './contractor-in.page.html',
  styleUrls: ['./contractor-in.page.scss'],
})
export class ContractorInPage implements OnInit {
  contractor = {
    firstName: '',
    lastName: '',
    company: '',
    phoneNumber: '',
    rego: '',
    area: ''
  };
  http : HttpClient;

  constructor(http: HttpClient, private router: Router) {
    this.http = http;
   }

  ngOnInit() {
 
  }
  signIn(form){
  
    fetch('https://api.smtp2go.com/v3/email/send', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "api_key": "api-38B578D6B91C11EBA64CF23C91C88F4E",
        "to": ["steve@christiespaces.com.au", "sandra@christiespaces.com.au"],
        "sender": "Christie Spaces Reception <no-reply@christiespaces.com.au>",
        "subject": "Contractor" + " " + this.contractor.firstName + " " + this.contractor.lastName + " " + "from" + " " + this.contractor.company + " " + "has signed in. WALKER STREET",
        "html_body": "Their Details are Below:" + "<br>" + "<br>" + this.contractor.firstName + " " + this.contractor.lastName + "<br> <strong>" + this.contractor.company + "</strong><br>" + "0" + this.contractor.phoneNumber + "<br>" + "Rego:" + " " + this.contractor.rego + "<br>" + "Work Area:" + " " + this.contractor.area,
      })
    }).then(function (response) {
      return response.json();
    });
    console.log(this.contractor.firstName, this.contractor.lastName, this.contractor.company, this.contractor.phoneNumber, this.contractor.rego, this.contractor.area),
      this.router.navigate(['/locations/contractor-in']);
  }

}
