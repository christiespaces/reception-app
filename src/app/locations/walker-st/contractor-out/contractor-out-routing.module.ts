import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContractorOutPage } from './contractor-out.page';

const routes: Routes = [
  {
    path: '',
    component: ContractorOutPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContractorOutPageRoutingModule {}
