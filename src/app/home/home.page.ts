import { Component, OnDestroy, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit  {
public images: string[] = [
'https://lh3.googleusercontent.com/pw/AM-JKLW-12Npqn0E8kFoZr1M9PyZEzPtS_eBgiD0hvmElaMG_uWqbo_IQ6w-uvKNC5KldidtYAQYIpOFFonbo_biV0N2Zm0kl2H8U2OGF4NhH9pxbg4upSMVbuwjfessm1q5NFXJd0xCLfI6Zbqt0NRUIgEC=w1454-h969-no', 
'https://lh3.googleusercontent.com/pw/AM-JKLWMLgQtnrHAqVWXhd0OZ-OgyVvfmWIu16ls00zi5VGeilcRbG_Cx7gt_6Hh20Mtog468Dw6P9XBl-GYwUTjUZ09UmiGgGnnPYjMOiPLUelFPZYLt1rVApxWsu-RN_w7Z0HJdPzMAOLekE2dGb2Vzoxy=w1451-h969-no', 
'https://lh3.googleusercontent.com/pw/AM-JKLVCdM1rhssmIhJgNQ6v5fzFs6GB6I0nCkvrEoAc1lLqgDILUXZbPDy2VRAjmO3DBpSM2JhTfCvctvjz5gTOpKcC80-ZLF-GZEbZV_ebtN-IhvoBHQQa7oUtX97LFtNmL5z94sWpdvHkGY5CPtH8RgB_=w1453-h969-no', 
'https://lh3.googleusercontent.com/pw/AM-JKLXItGKoDVMnqnEpjWUwOEYhgpK-4XLYiB4E7OOZv6tvaSXH2QcHEWn1CU1kIGbnui-cUj2TfDkesJDoXoKUr2xi6otX7I45LRHNPKME8hf9diuR_L9vR_-VHmD-pn6VMEtzR9-OEB-K5UVU_N5V7r5j=w1452-h969-no', 
'https://lh3.googleusercontent.com/pw/AM-JKLWhc-QtklvEoAbRlo0ZBh6ZVD_MZaKTwFiMF4mbBJ7XVhCLUYf0GsxoB2O04_mVyPJfaMOcl3Kgo0SMkWzz-4kmNz_uTqX5aPqiRVJmUhRHHBwAT5niB9hD4OW-xqhM_vg3ar38q8te7KrAXBu85-FR=w1483-h969-no', 
'https://lh3.googleusercontent.com/pw/AM-JKLVP2lfPTFRWEbBfzZagR61tUhdtzHIc659BCsfgrM3GRm8u2mIBivsj8o9nVp58Nj0lkpetN42O2-erq19tKeUEWwFTM58EKly_BBCKLaJ8po3_FJSX56XQ5MDQmK9vrHEFDylHRiNKQ6fw9QP6TC83=w1455-h969-no', 
'https://lh3.googleusercontent.com/pw/AM-JKLXAXraMbwPy9efPfiVjHMh4SKAk6kgaKPqNG365yKyGXSbr76Gx8utLdw6KYCiHgQl3ArLBy7e6Csb5Ais88jo7H-K4COj1RM7oHoa8bKt9CQtwzorMQ8yHvqXy7TXIonTMZTCYcLzVBKUIp8SB2JSF=w1454-h969-no'];
backgroundImage: string = '';


//Contstrcutor reloads the active route so the ngOnIint will fire again.
constructor(
  private _activatedRoute: ActivatedRoute
) {
  this._activatedRoute.paramMap.subscribe(params => {
      this.ngOnInit();
  });
}
 ngOnInit(){


  let ran = Math.round((Math.random()*100)%6);
  console.log(ran, Math.random()*100)
  this.backgroundImage = this.images[ran];

}




}

